import os
import re
import socket
import subprocess
from libqtile.config import Drag, Key, Screen, Group, Drag, Click, Rule
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
from libqtile.layout.xmonad import MonadWide

mod = "mod4"
mod1 = "alt"
mod2 = "control"
home = os.path.expanduser('~')
myterm ="alacritty"

@lazy.function
def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)

@lazy.function
def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)

keys = [
# Basic Qtile functions
    Key([mod], "e", lazy.spawn('code')),
    Key([mod], "f", lazy.window.toggle_fullscreen()),
    Key([mod], "q", lazy.window.kill()),
    Key([mod], "Escape", lazy.spawn('xkill')),
    Key([mod, "shift"], "r", lazy.restart()),       
    Key([mod, "shift"], "q", lazy.window.kill()),
    Key(["mod1", "control"], "o", lazy.spawn(home + '/.config/qtile/scripts/picom-toggle.sh')),
    Key([mod], "s", lazy.spawn(myterm + '-e /home/stefan/.config/qtile/scripts/qtile_keybinds')),
# Applications
    Key(["mod1"], "F3", lazy.spawn('xfce4-appfinder')),  
    Key(["mod1", "control"], "a", lazy.spawn('xfce4-appfinder')),
    Key(["mod1", "control"], "b", lazy.spawn('pcmanfm')),
    Key(["mod1", "control"], "f", lazy.spawn('firefox')),
    Key(["mod1", "control"], "g", lazy.spawn('steam')),
    Key(["mod1", "control"], "m", lazy.spawn('thunderbird')),
    Key(["mod1", "control"], "p", lazy.spawn('pamac-manager')),
    Key(["mod1", "control"], "s", lazy.spawn('xfce4-settings-manager')),
    Key(["mod1", "control"], "t", lazy.spawn(myterm)),
    Key(["mod1", "control"], "u", lazy.spawn('pavucontrol')),
    Key([mod, "shift"], "d", lazy.spawn("dmenu_run -g 4 -l 5 -i -nb '#292b2b' -nf '#00eeff' -sb '#00eeff' -sf '#292b2b' -fn 'NotoMonoRegular:bold:pixelsize=14'")),
    Key([mod, "shift"], "x", lazy.spawn('lxappearance')),
    Key([mod, "shift"], "Return", lazy.spawn('pcmanfm')),
    Key([mod], "Return", lazy.spawn(myterm)),
    Key([mod], "v", lazy.spawn('pavucontrol')),
    Key([mod], "F12", lazy.spawn('vlc --video-on-top')),
    #Key([mod, "shift"], "x", lazy.shutdown()),
# Variety specific Keybindings
    Key(["mod1"], "f", lazy.spawn('variety -f')),
    Key(["mod1"], "h", lazy.spawn('urxvt -e htop')),
    Key(["mod1"], "n", lazy.spawn('variety -n')),
    Key(["mod1"], "p", lazy.spawn('variety -p')),
    Key(["mod1"], "t", lazy.spawn('variety -t')),
    Key(["mod1"], "Up", lazy.spawn('variety --pause')),
    Key(["mod1"], "Down", lazy.spawn('variety --resume')),
    Key(["mod1"], "Left", lazy.spawn('variety -p')),
    Key(["mod1"], "Right", lazy.spawn('variety -n')),  
# VARIETY KEYS WITH PYWAL
    Key(["mod1", "shift"], "f", lazy.spawn(home + '/.config/qtile/scripts/set-pywal.sh -f')),
    Key(["mod1", "shift"], "p", lazy.spawn(home + '/.config/qtile/scripts/set-pywal.sh -p')),
    Key(["mod1", "shift"], "n", lazy.spawn(home + '/.config/qtile/scripts/set-pywal.sh -n')),
    Key(["mod1", "shift"], "u", lazy.spawn(home + '/.config/qtile/scripts/set-pywal.sh -u')),
# CONTROL + SHIFT KEYS
    Key([mod2, "shift"], "Escape", lazy.spawn('xfce4-taskmanager')),
# SCREENSHOTS
    Key([], "Print", lazy.spawn("scrot 'ArcoLinux-%Y-%m-%d-%s_screenshot_$wx$h.jpg' -e 'mv $f $$(xdg-user-dir PICTURES)'")),
    Key([mod2], "Print", lazy.spawn('xfce4-screenshooter')),
    Key([mod2, "shift"], "Print", lazy.spawn('gnome-screenshot -i')),
# INCREASE/DECREASE BRIGHTNESS
    Key([mod], "F5", lazy.spawn("blight set -5%")),
    Key([mod], "F6", lazy.spawn("blight set +5%")),
# INCREASE/DECREASE/MUTE VOLUME
    Key([], "XF86AudioMute", lazy.spawn("amixer -q set Master toggle")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("amixer -q set Master 5%-")),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer -q set Master 5%+")),
    Key([], "XF86AudioPlay", lazy.spawn("playerctl play-pause")),
    Key([], "XF86AudioNext", lazy.spawn("playerctl next")),
    Key([], "XF86AudioPrev", lazy.spawn("playerctl previous")),
    Key([], "XF86AudioStop", lazy.spawn("playerctl stop")),
#    Key([], "XF86AudioPlay", lazy.spawn("mpc toggle")),
#    Key([], "XF86AudioNext", lazy.spawn("mpc next")),
#    Key([], "XF86AudioPrev", lazy.spawn("mpc prev")),
#    Key([], "XF86AudioStop", lazy.spawn("mpc stop")),
# QTILE LAYOUT KEYS
    Key([mod], "n", lazy.layout.normalize()),
    Key([mod], "space", lazy.next_layout()),
# CHANGE FOCUS
    Key([mod], "k", lazy.layout.up()),
    Key([mod], "j", lazy.layout.down()),
    Key([mod], "h", lazy.layout.left()),
    Key([mod], "l", lazy.layout.right()),
# RESIZE UP, DOWN, LEFT, RIGHT
    Key([mod, "control"], "l",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
        ),
    Key([mod, "control"], "Right",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
        ),
    Key([mod, "control"], "h",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
        ),
    Key([mod, "control"], "Left",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
        ),
    Key([mod, "control"], "k",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
        ),
    Key([mod, "control"], "Up",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
        ),
    Key([mod, "control"], "j",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
        ),
    Key([mod, "control"], "Down",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
        ),
# FLIP LAYOUT FOR MONADTALL/MONADWIDE
    Key([mod, "shift"], "f", lazy.layout.flip()),
# FLIP LAYOUT FOR BSP
    Key([mod, "mod1"], "k", lazy.layout.flip_up()),
    Key([mod, "mod1"], "j", lazy.layout.flip_down()),
    Key([mod, "mod1"], "l", lazy.layout.flip_right()),
    Key([mod, "mod1"], "h", lazy.layout.flip_left()),
# MOVE WINDOWS UP OR DOWN BSP LAYOUT
    Key([mod, "shift"], "k", lazy.layout.shuffle_up()),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "h", lazy.layout.shuffle_left()),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right()),
# MOVE WINDOWS UP OR DOWN MONADTALL/MONADWIDE LAYOUT
    Key([mod, "shift"], "Up", lazy.layout.shuffle_up()),
    Key([mod, "shift"], "Down", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "Left", lazy.layout.swap_left()),
    Key([mod, "shift"], "Right", lazy.layout.swap_right()),
# TOGGLE FLOATING LAYOUT
    Key([mod, "shift"], "space", lazy.window.toggle_floating()
    ),
]
#group_names = [("WWW", {'layout': 'monadtall'}),
 #              ("DEV", {'layout': 'monadtall'}),
 #              ("SYS", {'layout': 'monadtall'}),
 #              ("DOC", {'layout': 'monadtall'}),
 #              ("MAIL", {'layout': 'monadtall'}),
 #              ("MUS", {'layout': 'monadtall'}),
 #              ("VID", {'layout': 'monadtall'}),
#groups = [Group(name, **kwargs) for name, kwargs in group_names]
#for i, (name, kwargs) in enumerate(group_names, 1):
 #   keys.append(Key([mod], str(i), lazy.group[name].toscreen()))        # Switch to another group
  #  keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(name))) # Send current window to another group
groups = []
# FOR QWERTY KEYBOARDS
group_names = ["1", "2", "3", "4", "5", "6", "7",]
group_labels = ["WWW", "DEV", "GAME", "TEXT", "FILE", "MAIL", "MUSI",]
group_layouts = ["monadtall", "treetab", "Max", "monadtall", "monadtall", "Max", "Max",]
for i in range(len(group_names)):
    groups.append(
        Group(
            name=group_names[i],
            layout=group_layouts[i].lower(),
            label=group_labels[i],
        ))

for i in groups:
    keys.extend([

#CHANGE WORKSPACES
        Key([mod], i.name, lazy.group[i.name].toscreen()),
        Key([mod], "Tab", lazy.screen.next_group()),
        Key(["mod1"], "Tab", lazy.screen.next_group()),
        Key(["mod1", "shift"], "Tab", lazy.screen.prev_group()),
# MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND STAY ON WORKSPACE
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name)),
# MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND FOLLOW MOVED WINDOW TO WORKSPACE
        #Key([mod, "shift"], i.name, lazy.window.togroup(i.name) , lazy.group[i.name].toscreen()),
    ])
layout_theme = {"margin":5,
            "border_width":2,
            "border_focus": "#5e81ac",
            "border_normal": "#4c566a"
            }
layouts = [
    layout.MonadTall(
        margin=8, 
        border_width=2, 
        border_focus="#5e81ac", 
        border_normal="#4c566a"),
    layout.Floating(**layout_theme),
    layout.MonadWide(**layout_theme),
    layout.Max(**layout_theme),
    layout.TreeTab(
         font = "Ubuntu Bold",
         fontsize = 12,
         sections = ["FIRST", "SECOND"],
         section_fontsize = 11,
         bg_color = "141414",
         active_bg = "90C435",
         active_fg = "000000",
         inactive_bg = "384323",
         inactive_fg = "a0a0a0",
         padding_y = 5,
         section_top = 10,
         panel_width = 320
         )
]
# COLORS FOR THE BAR
colors=[["#000000", "#000000"], # color 0
        ["#2F343F", "#2F343F"], # color 1
        ["#c0c5ce", "#c0c5ce"], # color 2
        ["#fba922", "#fba922"], # color 3
        ["#3384d0", "#3384d0"], # color 4
        ["#f3f4f5", "#f3f4f5"], # color 5
        ["#cd1f3f", "#cd1f3f"], # color 6
        ["#7CFF00", "#7CFF00"], # color 7
        ["#6790eb", "#6790eb"], # color 8
        ["#a9a9a9", "#a9a9a9"], # color 9
        ["#ff7f00", "#ff7f00"], # color 10
        ["#40e0d0", "#40e0d0"]] # color 11
prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())
# WIDGETS FOR THE BAR
widget_defaults = dict(font="Ubuntu Bold",
                fontsize = 12,
                padding = 2,
                background=colors[1])
def init_widgets_list():
    widgets_list = [
                widget.GroupBox(font="Ubuntu Bold",
                        fontsize = 12,
                        margin_y = 0,
                        margin_x = 0,
                        padding_y = 0,
                        padding_x = 5,
                        borderwidth = 0,
                        disable_drag = True,
                        active = colors[8],
                        inactive = colors[6],
                        rounded = False,
                        highlight_method = "text",
                        this_current_screen_border = colors[7],
                        foreground = colors[11],
                        background = colors[0]
                    ),
                widget.Sep(
                        linewidth = 2,
                        padding = 5,
                        foreground = colors[2],
                        background = colors[0],
                    ),
                widget.CurrentLayout(
                        font = "Ubuntu Bold",
                        foreground = colors[10],
                        background = colors[0],
                    ),
                widget.Sep(
                        linewidth = 2,
                        padding = 5,
                        foreground = colors[2],
                        background = colors[0],
                    ),
                widget.WindowName(font="Ubuntu Bold",
                        fontsize = 13,
                        foreground = colors[10],
                        background = colors[0],
                        padding = 5,
                    ),
                widget.Systray(
                    background=colors[0],
                    icon_size=20,
                    padding = 5
                    ),                
                widget.Sep(
                    linewidth = 2,
                    padding = 1,
                    foreground = colors[2],
                    background = colors[0]
                    ),
                widget.TextBox(
                        font="Ubuntu Bold",
                        text="  ",
                        foreground=colors[6],
                        background=colors[0],
                        padding = 0,
                        fontsize= 25,
                    ),             
                widget.Clock(
                        foreground = colors[11],
                        background = colors[0],
                        font="Ubuntu Bold",
                        fontsize = 13,
                        padding = 5,
                        format="%d-%m-%Y %H:%M",
                        mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn('google-calendar-dark')},
                    ),             
                widget.Sep(
                        linewidth = 2,
                        padding = 5,
                        foreground = colors[2],
                        background = colors[0],
                    ),
                widget.TextBox(
                        font="FontAwesome",
                        text="  ",
                        foreground = colors[4],
                        background=colors[0],
                        padding = 0,
                        fontsize=18,
                    ),
               widget.Memory(
                        font="Ubuntu Bold",
                        padding = 5,
                        fontsize = 13,
                        foreground = colors[11],
                        background = colors[0],
                        format = '{MemUsed}M/{MemTotal}M',
                        update_interval = 1,
                        mouse_callbacks = {'Button1':lambda qtile: qtile.cmd_spawn(myterm + ' -e htop')}
                    ),
               widget.Sep(
                        linewidth = 2,
                        padding = 5,
                        foreground = colors[2],
                        background = colors[0]
                    ),
               widget.Image(
                   background = colors[0],
                   filename = "~/.config/qtile/icons/battery_icons_horiz/battery-full-charge.png",
                   margin = 4,
                    ),
               widget.Battery(
                      background = colors[0], 
                      foreground = colors[11],
                      low_foreground = colors[6],
                      padding = 5,
                      font = "Ubuntu Bold",
                      fontsize = 13,
                      battery = "BAT0",
                      charge_char = "↑",
                      discharge_char = "↓",
                      empty_char = "0",
                      full_char = "=",
                      update_interval = 5,
                      mouse_callbacks ={'Button1': lambda qtile: qtile.cmd_spawn('xfce4-power-manager-settings')}
                    ),
               widget.Sep(
                      linewidth = 2,
                      padding = 1,
                      foreground = colors[2],
                      background = colors[0]
                    ),
               widget.Backlight(
                      font = "Ubuntu Bold",
                      fontsize = 13,
                      foreground=colors[11],
                      background=colors[0],
                      padding = 1,
                      backlight_name = "intel_backlight",
                    ),
                widget.Sep(
                      linewidth = 2,
                      padding = 1,
                      foreground = colors[2],
                      background = colors[0],
                    ),
               widget.Net(
                      font = "Ubuntu Bold",
                      fontsize = 13,
                      interface="wlan0",
                      foreground=colors[11],
                      background=colors[0],
                      padding = 5,
                      format = '{down} ↓↑ {up}',
                      mouse_callbacks ={'Button1': lambda qtile: qtile.cmd_spawn('nm-connection-editor')},
                    ),
               widget.Sep(
                      linewidth = 2,
                      padding = 5,
                      foreground = colors[2],
                      background = colors[0]
                    ),
            #    widget.Image(
            #        background = colors[0],
            #        filename = "~/.config/qtile/icons/temperature.png",
            #        margin = 4,
            #         ),      
            #    widget.ThermalSensor(
            #         background = colors[0], 
            #         foreground = colors[11],
            #         font = "Ubuntu Bold",
            #         foreground_alert = colors[6],
            #         metric = True,
            #         padding = 2,
            #         threshold = 80,
            #         mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn('psensor')}
            #         ),
                # widget.Sep(
                #     linewidth = 2,
                #     padding = 3,
                #     foreground = colors[2],
                #     background = colors[0]
                #     ),
               widget.TextBox(
                    font="FontAwesome",
                    text="  ",
                    foreground=colors[6],
                    background=colors[0],
                    padding = 0,
                    fontsize=18
                    ),
               widget.CPU(
                    background = colors[0], 
                    foreground = colors[11],
                    font = "Ubuntu Bold",
                    fontsize = 13,
                    padding= 5,
                    update_interval = 2.0,
                    mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn(myterm + ' -e bpytop')},
                    ),
               widget.ThermalSensor(
                    background = colors[0], 
                    foreground = colors[11],
                    font = "Ubuntu Bold",
                    foreground_alert = colors[6],
                    metric = True,
                    padding = 2,
                    threshold = 80,
                    mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn('psensor')}
                    ),
                widget.Sep(
                    linewidth = 2,
                    padding = 5,
                    foreground = colors[2],
                    background = colors[0],
                    ),
                # widget.TextBox(
                #        text = "Updates:",
                #        font = "Ubuntu Bold",
                #        fontsize = 13,
                #        padding = 5,
                #        mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn(myterm + ' -e yay -Syu --noconfirm')},
                #        foreground = colors[11],
                #        background = colors[0],
                #     ),
                widget.CheckUpdates(
                       background = colors[0], 
                       foreground = colors[11],
                       font = "Ubuntu Bold",
                       fontsize = 13,
                       colour_have_updates = colors[10],
                       colour_no_updates = colors[6],
                       mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn(myterm + ' -e yay -Syu --noconfirm')},
                       custom_command  = "checkupdates",
                       update_interval = "60",
                       display_format = "Updates:{updates}",
                       no_update_string ="NA",
                    ),
                     
                widget.Sep(
                    linewidth = 2,
                    padding = 5,
                    foreground = colors[2],
                    background = colors[0]
                    ),
                widget.Volume(
                       background = colors[0], 
                       foreground = colors[11],
                       padding = 5,
                       font = "Ubuntu Bold",
                       fontsize = 13,
                       ),
] 
    return widgets_list

widgets_list = init_widgets_list()

def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    return widgets_screen1

def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    return widgets_screen2

widgets_screen1 = init_widgets_screen1()
widgets_screen2 = init_widgets_screen2()

def init_screens():
    return [Screen(top=bar.Bar(widgets=init_widgets_screen1(), size=24, opacity= 0.75)),
            Screen(top=bar.Bar(widgets=init_widgets_screen2(), size=24, opacity= 0.75))]
screens = init_screens()


# MOUSE CONFIGURATION
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size())
]

dgroups_key_binder = None
dgroups_app_rules = []

# ASSIGN APPLICATIONS TO A SPECIFIC GROUPNAME
# BEGIN

@hook.subscribe.client_new
def assign_app_group(client):
     d = {}
    #########################################################
    ################ assgin apps to groups ##################
    #########################################################
     d["1"] = ["Navigator", "firefox", "stable","brave", "brave-browser", ]
     d["2"] = ["geany","code-oss", "code", "terminator", "termite", "xterm", "Alacritty", ]
     d["3"] = ["Steam", "lutris" ]
     d["4"] = ["DesktopEditors", "joplin",]
     d["5"] = ["pcmanfm", "thunar", ]
     d["6"] = ["Mail", "Thunderbird", ]
     d["7"] = ["clementine"]
 #   ##########################################################
     wm_class = client.window.get_wm_class()[0]

     for i in range(len(d)):
         if wm_class in list(d.values())[i]:
             group = list(d.keys())[i]
             client.togroup(group)
             client.group.cmd_toscreen()

# END
# ASSIGN APPLICATIONS TO A SPECIFIC GROUPNAME
main = None

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/scripts/autostart.sh'])

@hook.subscribe.startup
def start_always():
    # Set the cursor to something sane in X
    subprocess.Popen(['xsetroot', '-cursor_name', 'left_ptr'])

@hook.subscribe.client_new
def set_floating(window):
    if (window.window.get_wm_transient_for()
            or window.window.get_wm_type() in floating_types):
        window.floating = True

floating_types = ["notification", "toolbar", "splash", "dialog"]


follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    {'wmclass': 'Arcolinux-welcome-app.py'},
    {'wmclass': 'Arcolinux-tweak-tool.py'},
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},
    {'wmclass': 'makebranch'},
    {'wmclass': 'maketag'},
    {'wmclass': 'Arandr'},
    {'wmclass': 'feh'},
    {'wmclass': 'Galculator'},
    {'wmclass': 'arcolinux-logout'},
    {'wmclass': 'xfce4-terminal'},
    {'wname': 'branchdialog'},
    {'wname': 'Open File'},
    {'wname': 'pinentry'},
    {'wmclass': 'ssh-askpass'},
    {'wmclass': 'google-calendar-dark'},
    {'wmclass': 'psensor'},
    {'wmclass': 'nm-connection-editor'},
    {'wmclass': 'xfce4-power-manager-settings'},
    {'wmclass': 'grub-customizer'},
    #{'wmclass': 'Steam'}
])
auto_fullscreen = True
focus_on_window_activation = "smart" # or focus

wmname = "LG3D"
