#! /bin/sh

bspc rule -r "*"

# Autostart
variety &
nm-applet &
sxhkd &
solaar &
numlockx on &
blueberry-tray &
picom --config $HOME/.config/picom/picom.conf &
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
/usr/lib/xfce4/notifyd/xfce4-notifyd & 
volumeicon &
polybar&

killall bspswallow
pidof bspswallow | bspswallow &

# Monitor setup

bspc monitor eDP-1 -d 1 2 3 4 5 6 7 8 9
bspc monitor DVI-I-1-1 -d 1 2 3 4 5 6 7 8 9
bspc monitor DVI-I-2-2 -d 1 2 3 4 5 6 7 8 9
#bspc monitor HDMI-0 -d 1 2 3 4 5 6 7 8 9
#bspc monitor DisplayPort-1 -d 1 2 3 4 5 6 7 8 9

## Bspwm config

bspc config remove_disabled_monitors      true
bspc config merge_overlapping_monitors    true

###---Monitor And Desktop Settings----###
bspc config top_padding 32
bspc config bottom_padding 0
bspc config left_padding 0
bspc config right_padding 0
bspc config border_width 2
bspc config window_gap 10

#polybar hidden when fullscreen for vlc, youtube, mpv ...
#find out the name of your monitor with xrandr
xdo below -t $(xdo id -n root) $(xdo id -a polybar-main_eDP-1)
xdo below -t $(xdo id -n root) $(xdo id -a polybar-main_DVI-I-1-1)
xdo below -t $(xdo id -n root) $(xdo id -a polybar-main_DVI-I-2-2)
#xdo below -t $(xdo id -n root) $(xdo id -a polybar-main_DVI-I-1-1)
#xdo below -t $(xdo id -n root) $(xdo id -a polybar-main_DVI-I-2-2)

###---Rules---###
